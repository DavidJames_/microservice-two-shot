import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';



function HatList() {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/');

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    return (
        <div key='container' className='container my-5'>
            <div key="welcome" className='text-center'>
                <h1 className="display-5 fw-bold text-center">HATS!</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4 text-center">
                        Keep track of your hats here!
                    </p>
                    <NavLink aria-current='page' to='new'><button className="btn btn-primary btn-block btn-md m-2">Create a new hat</button></NavLink>
                </div>
            </div>
            <div className="row justify-content-md-center">
                {hats.map(hats => {
                    return (
                        <div key={hats.href} className='card m-3 p-3 text-center shadow-lg' style={{width: '20rem'}}>
                            <img width='360' height='240' src={hats.image} alt='img not found' className='card-img-top p-2 my-2 border shadow-lg'/>
                            <div key='attributelist' className='card-body'>
                                <ul className="list-group list-group-flush border-top">
                                    <li key='fabric' className="list-group-item">Fabric - {hats.fabric}</li>
                                    <li key='style' className="list-group-item">Style - {hats.style}</li>
                                    <li key='color' className="list-group-item">Color - {hats.color}</li>
                                </ul>
                                <div key='locationlist' className='justify-content-md-center m-2 p-2 border-top border-bottom'>
                                    <h6>{hats.location.closet_name}</h6>
                                    <div key="sectionNum" className='d-inline p-2 text-muted'>Section - {hats.location.section_number}</div>
                                    <div key='shelfNum' className='d-inline p-2 text-muted'>Shelf - {hats.location.shelf_number}</div>
                                </div>
                            </div>
                            <div className='row justify-content-md-center m-2'>
                                    <button href="#" className="btn btn-primary btn-block btn-md m-2">Edit</button>
                                    <button onClick={() => {
                                        fetch('http://localhost:8090/api/hats/' + hats.id, {method: 'DELETE'})
                                        .then(() => {
                                            getData()
                                        })}} className="btn btn-danger btn-block btn-md m-2">Delete</button>
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default HatList
