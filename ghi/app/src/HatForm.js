import React, {useEffect, useState,} from 'react';


function HatForm() {
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [image, setImage] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.location = location;
        data.fabric = fabric;
        data.style = style;
        data.color = color;
        data.image = image;

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setLocation('');
            setFabric('');
            setStyle('');
            setColor('');
            setImage('');

        }
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleImageChange = (event) => {
        const value = event.target.value;
        setImage(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }
    const fetchData = async () => {
        const url='http://localhost:8100/api/locations/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 text-center">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new hat</h1>
                        <form onSubmit={handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input value={style} onChange={handleStyleChange} placeholder="Style" required type="style" name="style" id="style" className="form-control"></input>
                                <label htmlFor="name">Style</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"></input>
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"></input>
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={image} onChange={handleImageChange} placeholder="Image URL" required type="text" name="image" id="image" className="form-control"></input>
                                <label htmlFor="image">Image URL</label>
                            </div>
                            <div className="mb-3">
                                <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                                    <option value="">Choose a closet</option>
                                    {locations.map(location => {
                                        return (
                                            <option key={location.href} value={location.href}>
                                                <div>Closet: {location.closet_name} Section: {location.section_number} Shelf: {location.shelf_number}</div>
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default HatForm;
