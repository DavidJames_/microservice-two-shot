import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function ShoeList() {
    const [shoes, setShoes] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
        }
    }

    
    useEffect(()=>{
        getData()
    }, [])
    

    return (
        <div className='my-5'>
            <div>
                <h1 className="display-5 fw-bold text-center">SHOES!</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4 text-center">
                        Keep track of your shoes here!
                    </p>
                </div>
                <div className="text-center">
                    <NavLink to='new' type="button" href="#" className="btn btn-primary btn-block btn-md m-2">Create a new shoe</NavLink>
                </div>
            </div>
            <div className='container'>
                <div className="row justify-content-md-center">
                    {shoes.map(shoe => {
                        return (
                            <div key={shoe.href} className='card m-2 shadow-md bg-blue ' style={{width: '20rem'}}>
                                <img width='360' height='240' src={shoe.picture_url} alt='img not found' className='card-img-top my-2 border'/>
                                <div className='card-body'>
                                    <ul className="list-group list-group-flush border-top">
                                        <li className="list-group-item">Manufacturer - {shoe.manufacturer}</li>
                                        <li className="list-group-item">Model Name - {shoe.model_name}</li>
                                        <li className="list-group-item">Color - {shoe.color}</li>
                                    </ul>
                                    <div className='justify-content-md-center m-2 p-2 border-top border-bottom'>
                                        <h6>{shoe.bin.closet_name}</h6>
                                        <div className='d-inline p-2 text-muted'>Bin number - {shoe.bin.bin_number}</div>
                                        <div className='d-inline p-2 text-muted'>Bin size - {shoe.bin.bin_size}</div>
                                    </div>
                                    <row className='row justify-content-md-center'>
                                        <button onClick={() => {
                                            fetch('http://localhost:8080/api/shoes/' + shoe.id, {method: 'DELETE'})
                                            .then(() => {
                                                getData()
                                            })}} className="btn btn-danger btn-block btn-md m-2">Delete</button>
                                    </row>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}
export default ShoeList







