# Wardrobify

Team:

* David - Hats
* Dane - Shoes

## Design
We're using Bootstrap
## Shoes microservice

Created a Shoe model as well as a BinVO to import bin data from seperate microservice (wardrobe-api)

Made view functions for a list of shoes as well as a shoe detail (+ JSON Encoders)
* view functions include POST, GET, PUT, and DEL HTTP Requests respectively

Then, I created a form for creating a shoe as well as a shoe list page via the REACT framework using JSX(pre-installed)

Implemented the REACT JSX files into the App.js via NavLinks 

Implemented a button to delete a shoe directly on the list, attached to the respective shoe
* deletes both on the webpage itself as well as in the database

Finally, implemented a "Creat a new shoe" button right above the list of shoes that redirects to the HatForm
* added some styling via Bootstrap to enhance user experience 


## Hats microservice

Created Hat model and LocationVO model, several encoders. 

Added list functionality as well as edit(WIP), delete, and create hat functionality. 

Polled data for my LocationVO to feed stored location into my hat details. 

Added REACT functionality and made use of Bootstrap.
