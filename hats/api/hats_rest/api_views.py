from django.shortcuts import render
from .models import Hats, LocationVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
# Create your views here.


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "shelf_number",
        "section_number",
        ]


class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "image",
        "location"
        ]
    encoders = {
        "location": LocationVOEncoder()
    }


class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "image",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatListEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_hats(request, id):
    if request.method == "GET":
        hats = Hats.objects.get(id=id)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            hats = Hats.objects.get(id=id)
            hats.delete()
            return JsonResponse(
                hats,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        content = json.loads(request.body)
        Hats.objects.filter(id=id).update(**content)
        hats = Hats.objects.get(id=id)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False
        )
